﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;

namespace HomeWork11
{
    class Program
    {
        static void Main(string[] args)
        {
			using (var connection = GetRabbitConnection())
			using (var channel = connection.CreateModel())
			{
				channel.QueueDeclare("HomeWork11-queue", true, false, false);

				var consumer = new EventingBasicConsumer(channel);
				consumer.Received += (model, ea) =>
				{
					var body = ea.Body;
					var message = Encoding.UTF8.GetString(body.ToArray());
					Console.WriteLine(" [x] Received {0}", message);
				};
				channel.BasicConsume(queue: "HomeWork11-queue",
									 autoAck: true,
									 consumer: consumer);

				Console.WriteLine(" Press [enter] to exit.");
				Console.ReadLine();
			}

		}

		static private IConnection GetRabbitConnection()
		{
			ConnectionFactory factory = new ConnectionFactory
			{
				UserName = "auddpfru",
				Password = "xDSi2pFPhkzJPctbPh5SPDJjfm0TroVN",
				VirtualHost = "auddpfru",
				HostName = "fish.rmq.cloudamqp.com"
			};
			IConnection conn = factory.CreateConnection();
			return conn;
		}

	}
}
