﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
			using (var connection = GetRabbitConnection())
			using (var channel = connection.CreateModel())
			{
				string message = "Test message in HomeWork11";
				var body = Encoding.UTF8.GetBytes(message);

				channel.BasicPublish(exchange: "HomeWork11-exchange",
									 routingKey: "HomeWork11-key",
									 basicProperties: null,
									 body: body);
				Console.WriteLine(" [x] Sent {0}", message);
			}

			Console.WriteLine(" Press [enter] to exit.");
			Console.ReadLine();

		}

		static private IConnection GetRabbitConnection()
		{
			ConnectionFactory factory = new ConnectionFactory
			{
				UserName = "auddpfru",
				Password = "xDSi2pFPhkzJPctbPh5SPDJjfm0TroVN",
				VirtualHost = "auddpfru",
				HostName = "fish.rmq.cloudamqp.com"
			};
			IConnection conn = factory.CreateConnection();
			return conn;
		}

	}
}
